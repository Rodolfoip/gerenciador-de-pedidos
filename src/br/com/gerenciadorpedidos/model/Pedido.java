package br.com.gerenciadorpedidos.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Pedido implements Serializable {

	private static final long serialVersion = 1L;
	// atributos
	private int codigo;
	private double valor;
	private int quantidade;
	private Campanha campanha;
	private Cliente cliente;
	private ArrayList<String> produtos;

	public Pedido(int codigo, double valor, int quantidade, Campanha campanha, Cliente cliente) {
		this.codigo = codigo;
		this.valor = valor;
		this.quantidade = quantidade;
		this.campanha = campanha;
		this.cliente = cliente;
		this.produtos = new ArrayList<>();
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public Campanha getCampanha() {
		return campanha;
	}

	public void setCampanha(Campanha campanha) {
		this.campanha = campanha;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public ArrayList<String> getProdutos() {
		return produtos;
	}

	public void setProdutos(ArrayList<String> produtos) {
		this.produtos = produtos;
	}

}
