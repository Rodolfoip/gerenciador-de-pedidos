package br.com.gerenciadorpedidos.model;

import java.io.Serializable;
import java.util.Date;

public class Campanha implements Serializable {

	private static final long serialVersionUID = 1L;
	// atributos
	private int numero;
	private Date dataFechamento;
	private Date dataEntrega;

	public Campanha(int numero, Date dataFechamento, Date dataEntrega) {
		this.numero = numero;
		this.dataFechamento = dataFechamento;
		this.dataEntrega = dataEntrega;

	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public Date getDataFechamento() {
		return dataFechamento;
	}

	public void setDataFechamento(Date dataFechamento) {
		this.dataFechamento = dataFechamento;
	}

	public Date getDataEntrega() {
		return dataEntrega;
	}

	public void setDataEntrega(Date dataEntrega) {
		this.dataEntrega = dataEntrega;
	}

}
