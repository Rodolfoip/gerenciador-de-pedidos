package br.com.gerenciadorpedidos.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;

public class Fatura implements Serializable {

	private static final long serialVersionUID = 1L;
	// atributos
	private int codigo;
	private Date dataPagamento;
	private ArrayList<Pedido> pedidos;
	private Campanha campanha;

	public Fatura(int codigo, Date dataPagamento, Campanha campanha) {
		this.codigo = codigo;
		this.dataPagamento = dataPagamento;
		this.campanha = campanha;
		this.pedidos = new ArrayList<>();
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public ArrayList<Pedido> getpedidos() {
		return pedidos;
	}

	public void setpedidos(ArrayList<Pedido> pedidos) {
		this.pedidos = pedidos;
	}

	public Campanha getCampanha() {
		return campanha;
	}

	public void setCampanha(Campanha campanha) {
		this.campanha = campanha;
	}

}
