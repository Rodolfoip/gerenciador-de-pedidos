package br.com.gerenciadorpedidos.view.campanha;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class TelaCampanhaInicio extends JFrame {
	// componentes
	private JButton btCadastro;
	private JButton btAlterar;
	private JButton btExcluir;
	private JButton btListar;
	// tamanhoBTS
	private Dimension tamanhoBotao = new Dimension(150, 50);

	public TelaCampanhaInicio() {
		super("Campanha inicio - Gerenciador de pedidos");
		init();
	}

	public void init() {
		// TEMA
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (IllegalAccessException e) {
			// TODO
		} catch (ClassNotFoundException e) {
			// TODO
		} catch (InstantiationException e) {
			// TODO
		} catch (UnsupportedLookAndFeelException e) {
			// TODO
		}
		Container container = getContentPane();
		container.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		// COMPONENTES
		// btCadasto
		btCadastro = new JButton("Cadastrar campanha");
		btCadastro.setPreferredSize(tamanhoBotao);
		constraints.gridx = 0;
		constraints.gridy = 0;
		container.add(btCadastro, constraints);

		// btAlterar
		btAlterar = new JButton("Alterar campanha");
		btAlterar.setPreferredSize(tamanhoBotao);
		constraints.gridx = 0;
		constraints.gridy = 1;
		container.add(btAlterar, constraints);

		// btExcluir
		btExcluir = new JButton("Excluir campanha");
		btExcluir.setPreferredSize(tamanhoBotao);
		constraints.gridx = 0;
		constraints.gridy = 2;
		container.add(btExcluir, constraints);

		// btListar
		btListar = new JButton("Tabela de campanhas");
		btListar.setPreferredSize(tamanhoBotao);
		constraints.gridx = 0;
		constraints.gridy = 3;
		container.add(btListar, constraints);

		// configsJframe
		setSize(500, 500);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	private class GerenciadorDeBotoes implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {

		}

	}
}
