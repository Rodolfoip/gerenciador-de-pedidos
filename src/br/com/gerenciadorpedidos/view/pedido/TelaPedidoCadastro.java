package br.com.gerenciadorpedidos.view.pedido;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class TelaPedidoCadastro extends JFrame {
	// componente
	private JTable tabelaClientes;
	private JTextField tfCampoPesquisa;
	private JLabel lbDigiteNome;
	private JButton btPesquisa;
	// tamanhoElementos
	private Dimension tamanhoBotao = new Dimension(152, 40);
	private Dimension tamanhoCampo = new Dimension(150, 20);

	public TelaPedidoCadastro() {
		super("Cadastro pedido - Gerenciador de pedidos");
		init();
	}

	public void init() {
		// TEMA
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (IllegalAccessException e) {
			// TODO
		} catch (ClassNotFoundException e) {
			// TODO
		} catch (InstantiationException e) {
			// TODO
		} catch (UnsupportedLookAndFeelException e) {
			// TODO
		}
		Container container = getContentPane();
		container.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		// COMPONENTES
		// tabela
		tabelaClientes = new JTable();

		// lbDigiteNome
		lbDigiteNome = new JLabel("Digite o nome do cliente");
		lbDigiteNome.setPreferredSize(tamanhoCampo);
		lbDigiteNome.setHorizontalAlignment(0);
		constraints.gridx = 0;
		constraints.gridy = 0;
		container.add(lbDigiteNome, constraints);
		
		// tfCampoPesquisa
		tfCampoPesquisa = new JTextField();
		tfCampoPesquisa.setPreferredSize(tamanhoCampo);
		constraints.gridx = 0;
		constraints.gridy = 1;
		container.add(tfCampoPesquisa, constraints);
		
		// btPesquisa
		btPesquisa = new JButton("Pesquisar");
		btPesquisa.setPreferredSize(tamanhoBotao);
		constraints.gridx = 0;
		constraints.gridy = 2;
		container.add(btPesquisa, constraints);
		
		// configsJframe
		setSize(1000, 700);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	private class GerenciadorDeBotoes implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			
		}
		
	}
}
