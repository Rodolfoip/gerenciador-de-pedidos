package br.com.gerenciadorpedidos.view.cliente;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import br.com.gerenciadorpedidos.control.ControladorCliente;

public class TelaClienteInicio extends JFrame {
	private ControladorCliente ctrlCliente;
	private GerenciadorDeBotoes gBtn;
	// componentes
	private JButton btCadastro;
	private JButton btAlterar;
	private JButton btExcluir;
	private JButton btListar;
	// tamanhoBTS
	private Dimension tamanhoBotao = new Dimension(170, 50);

	public TelaClienteInicio(ControladorCliente owner) {
		super("Cliente inicio - Gerenciador de pedidos");
		this.ctrlCliente = owner;
		this.gBtn = new GerenciadorDeBotoes();
		init();
	}

	public void init() {
		// TEMA
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (IllegalAccessException e) {
			// TODO
		} catch (ClassNotFoundException e) {
			// TODO
		} catch (InstantiationException e) {
			// TODO
		} catch (UnsupportedLookAndFeelException e) {
			// TODO
		}

		Container container = getContentPane();
		container.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		// COMPONENTES
		// btCadasto
		btCadastro = new JButton("Cadastrar cliente");
		btCadastro.setPreferredSize(tamanhoBotao);
		btCadastro.setActionCommand("cadastro");
		btCadastro.addActionListener(gBtn);
		constraints.gridx = 0;
		constraints.gridy = 0;
		container.add(btCadastro, constraints);

		// btAlterar
		btAlterar = new JButton("Alterar cliente");
		btAlterar.setPreferredSize(tamanhoBotao);
		constraints.gridx = 0;
		constraints.gridy = 1;
		container.add(btAlterar, constraints);

		// btExcluir
		btExcluir = new JButton("Excluir cliente");
		btExcluir.setPreferredSize(tamanhoBotao);
		constraints.gridx = 0;
		constraints.gridy = 2;
		container.add(btExcluir, constraints);

		// btListar
		btListar = new JButton("Tabela de clientes");
		btListar.setPreferredSize(tamanhoBotao);
		constraints.gridx = 0;
		constraints.gridy = 3;
		container.add(btListar, constraints);

		// configsJframe
		setSize(1000, 800);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	private class GerenciadorDeBotoes implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getActionCommand().equals("cadastro")){
				ctrlCliente.telaCadastro();
			}
		}

	}
}
