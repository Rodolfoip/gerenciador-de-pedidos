/**
 * 
 */
package br.com.gerenciadorpedidos.view.cliente;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;

import com.sun.javafx.scene.control.ControlAcceleratorSupport;

import br.com.gerenciadorpedidos.control.ControladorCliente;

/**
 * @author Rodolfo Ilce Pereira Email: rodolfo.pereirailce@gmail.com 15:11:09
 */
public class TelaClienteLista extends JFrame {
	private ControladorCliente ctrlCliente;
	private GerenciadorDeBotoes gBtn;
	// componentes
	private String[] colunas = { "Codigo" + "Nome", "Telefone" };
	private JButton btExcluir;
	private JButton btPesquisar;
	private JButton btAlterar;
	private DefaultTableModel tableModelCliente;
	private JTable tabelaClientes;
	// tamanho BTS
	private Dimension tamanhoBotao = new Dimension(150, 50);

	public TelaClienteLista(ControladorCliente owner) {
		super("Cliente tabela - Gerenciador de pedidos");
		this.ctrlCliente = owner;
		this.gBtn = new GerenciadorDeBotoes();
		this.tableModelCliente = new DefaultTableModel(colunas, 0);
		init();
	}

	public void init() {
		// TEMA
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (IllegalAccessException e) {
			// TODO
		} catch (ClassNotFoundException e) {
			// TODO
		} catch (InstantiationException e) {
			// TODO
		} catch (UnsupportedLookAndFeelException e) {
			// TODO
		}
		
		Container container = getContentPane();
		container.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		// COMPONENTES
		
	}

	private class GerenciadorDeBotoes implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

		}

	}
}
