/**
 * 
 */
package br.com.gerenciadorpedidos.view.cliente;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import br.com.gerenciadorpedidos.control.ControladorCliente;

/**
 * @author Rodolfo Ilce Pereira Email: rodolfo.pereirailce@gmail.com 18:26:05
 */
public class TelaClienteCadastro extends JFrame {
	private ControladorCliente ctrlCliente;
	private GerenciadorDeBotoes gBtn;
	// componentes
	private JLabel lbNome;
	private JLabel lbTelefone;
	private JTextField tfNome;
	private JTextField tfTelefone;
	private JButton btSalvar;
	private JButton btCancelar;

	// tamanho Componentes
	private Dimension tamanhoBotao = new Dimension(160, 50);
	private Dimension tamanhoTextField = new Dimension(150, 20);

	public TelaClienteCadastro(ControladorCliente owner) {
		super("Cliente cadastro - Gerenciador de pedidos");
		this.ctrlCliente = owner;
		this.gBtn = new GerenciadorDeBotoes();
		init();

	}

	public void init() {
		// TEMA
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (IllegalAccessException e) {
			// TODO
		} catch (ClassNotFoundException e) {
			// TODO
		} catch (InstantiationException e) {
			// TODO
		} catch (UnsupportedLookAndFeelException e) {
			// TODO
		}

		Container container = getContentPane();
		container.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		// COMPONENTES
		// lbNome
		lbNome = new JLabel("Nome:");
		constraints.gridx = 0;
		constraints.gridy = 0;
		container.add(lbNome, constraints);

		// lbTelefone
		lbTelefone = new JLabel("Telefone: ");
		constraints.gridx = 0;
		constraints.gridy = 1;
		container.add(lbTelefone, constraints);

		// tfNome
		tfNome = new JTextField("");
		tfNome.setPreferredSize(tamanhoTextField);
		constraints.gridx = 1;
		constraints.gridy = 0;
		container.add(tfNome, constraints);

		// tfTelefone
		tfTelefone = new JTextField();
		tfTelefone.setPreferredSize(tamanhoTextField);
		constraints.gridx = 1;
		constraints.gridy = 1;
		container.add(tfTelefone, constraints);

		// btSalvar
		btSalvar = new JButton("Salvar");
		btSalvar.setPreferredSize(tamanhoBotao);
		btSalvar.setActionCommand("salvar");
		btSalvar.addActionListener(gBtn);
		constraints.gridx = 1;
		constraints.gridy = 2;
		
		container.add(btSalvar, constraints);

		// btCancelar
		btCancelar = new JButton("Cancelar");
		btCancelar.setPreferredSize(tamanhoBotao);
		constraints.gridx = 0;
		constraints.gridy = 2;
		container.add(btCancelar, constraints);

		// configsJframe
		setSize(1000, 800);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	private class GerenciadorDeBotoes implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
//			if(e.getActionCommand().equals("salvar")) {
//				try {
//					ctrlCliente.cadastraCliente(tfNome.getText(), tfTelefone.getText());
//					dispose();
//				} catch (Exception e1) {
//					JOptionPane.showMessageDialog(null, e1.getMessage());
//				}
//			}
		}
		
	}
}
