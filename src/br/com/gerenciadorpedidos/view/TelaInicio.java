package br.com.gerenciadorpedidos.view;

import br.com.gerenciadorpedidos.control.ControladorGeral;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class TelaInicio extends JFrame {
	private ControladorGeral ctrlGeral;
	private GerenciadorBotoes gBtn;
	// componentes
	private JButton btCliente;
	private JButton btPedido;
	private JButton btCampanha;
	private JButton btFatura;
	// tamanhoBTS
	private Dimension tamanhoBotao = new Dimension(200, 50);

	public TelaInicio(ControladorGeral owner) {
		super("Inicio - Gerenciador de pedidos");
		this.ctrlGeral = owner;
		this.gBtn = new GerenciadorBotoes();
		init();
	}

	public void init() {
		// TEMA
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (IllegalAccessException e) {
			// TODO
		} catch (ClassNotFoundException e) {
			// TODO
		} catch (InstantiationException e) {
			// TODO
		} catch (UnsupportedLookAndFeelException e) {
			// TODO
		}

		Container container = getContentPane();
		container.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		// COMPONENTES
		// btCliente
		btCliente = new JButton();
		btCliente.setText("Cliente");
		btCliente.setPreferredSize(tamanhoBotao);
		btCliente.setActionCommand("cliente");
		btCliente.addActionListener(gBtn);
		constraints.gridx = 0;
		constraints.gridy = 0;
		container.add(btCliente, constraints);

		// btPedido
		btPedido = new JButton();
		btPedido.setText("Pedido");
		btPedido.setPreferredSize(tamanhoBotao);
		btPedido.setActionCommand("pedido");
		btPedido.addActionListener(gBtn);
		constraints.gridx = 0;
		constraints.gridy = 1;
		container.add(btPedido, constraints);

		// btCampanha
		btCampanha = new JButton();
		btCampanha.setText("Campanha");
		btCampanha.setPreferredSize(tamanhoBotao);
		btCampanha.setActionCommand("campanha");
		btCampanha.addActionListener(gBtn);
		constraints.gridx = 1;
		constraints.gridy = 0;
		container.add(btCampanha, constraints);

		// btFatura
		btFatura = new JButton();
		btFatura.setText("Fatura");
		btFatura.setPreferredSize(tamanhoBotao);
		btFatura.setActionCommand("fatura");
		btFatura.addActionListener(gBtn);
		constraints.gridx = 1;
		constraints.gridy = 1;
		container.add(btFatura, constraints);

		// configsJframe
		setSize(1000, 800);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private class GerenciadorBotoes implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getActionCommand().equals("cliente")){
				ctrlGeral.getCtrlCliente().telaInicio();
			}else if (e.getActionCommand().equals("pedido")){
				
			}else if (e.getActionCommand().equals("fatura")){

			}else if (e.getActionCommand().equals("campanha")){

			}
		}
		
	}
}
