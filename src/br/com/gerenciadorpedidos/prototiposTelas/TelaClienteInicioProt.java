/**
 * 
 */
package br.com.gerenciadorpedidos.prototiposTelas;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import br.com.gerenciadorpedidos.control.ControladorCliente;

import javax.swing.JButton;
import java.awt.Color;

/**
 * @author Rodolfo Ilce Pereira Email: rodolfo.pereirailce@gmail.com 09:02:02
 */
public class TelaClienteInicioProt extends JFrame {
	private ControladorCliente ctrlCliente;
	private GerenciadorBotoes gBtn;
	// componentes
	private JPanel contentPane;

	// /**
	// * Launch the application.
	// */
	// public static void main(String[] args) {
	// EventQueue.invokeLater(new Runnable() {
	// public void run() {
	// try {
	// TelaClienteInicioProt frame = new TelaClienteInicioProt();
	// frame.setVisible(true);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// });
	// }
	//
	// /**
	// * Create the frame.
	// */
	public TelaClienteInicioProt(ControladorCliente owner) {
		super("Cliente inicio - Gerenciador de pedidos");
		this.ctrlCliente = owner;
		this.gBtn = new GerenciadorBotoes();
		init();

	}

	public void init() {
		// config Jframe
		contentPane = new JPanel();
		contentPane.setBackground(new Color(139, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 559, 422);

		// Componentes
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.setBounds(196, 104, 150, 50);
		btnCadastrar.setActionCommand("cadastro");
		btnCadastrar.addActionListener(gBtn);
		contentPane.add(btnCadastrar);

		JButton btnListar = new JButton("Listar");
		btnListar.setBounds(196, 165, 150, 50);
		btnListar.setActionCommand("tabela");
		btnListar.addActionListener(gBtn);
		contentPane.add(btnListar);
	}

	private class GerenciadorBotoes implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals("cadastro")) {
				ctrlCliente.telaCadastroProt();
			} else if (e.getActionCommand().equals("tabela")) {
				ctrlCliente.telaTabelaProt();
			}
		}

	}
}
