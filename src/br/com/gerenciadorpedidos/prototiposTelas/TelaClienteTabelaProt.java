/**
 * 
 */
package br.com.gerenciadorpedidos.prototiposTelas;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import br.com.gerenciadorpedidos.control.ControladorCliente;
import br.com.gerenciadorpedidos.model.Cliente;

/**
 * @author Rodolfo Ilce Pereira Email: rodolfo.pereirailce@gmail.com 09:31:16
 */
public class TelaClienteTabelaProt extends JFrame {
	private ControladorCliente ctrlCliente;
	private GerenciadorDeBotoes gBtn;
	// componentes
	private JPanel contentPane;
	private JTable tabelaCliente;
	private DefaultTableModel tableModelCliente;
	private String[] colunas = { "Codigo", "Nome", "Telefone" };

	// /**
	// * Launch the application.
	// */
	// public static void main(String[] args) {
	// EventQueue.invokeLater(new Runnable() {
	// public void run() {
	// try {
	// TelaClienteTabelaProt frame = new TelaClienteTabelaProt();
	// frame.setVisible(true);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// });
	// }
	//
	// /**
	// * Create the frame.
	// */
	public TelaClienteTabelaProt(ControladorCliente owner) {
		super("Cliente tabela - Gerenciador de pedidos");
		this.ctrlCliente = owner;
		this.gBtn = new GerenciadorDeBotoes();
		init();

	}

	public void init() {
		contentPane = new JPanel();
		contentPane.setBackground(new Color(139, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		// Componentes
		// Tabela
		tableModelCliente = new DefaultTableModel(colunas, 0);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(42, 40, 350, 180);
		contentPane.add(scrollPane);

		atualizaTabela();

		tabelaCliente = new JTable();
		tabelaCliente.setModel(tableModelCliente);
		scrollPane.setViewportView(tabelaCliente);

		JLabel lblTabelaDeClientes = new JLabel("Tabela de clientes");
		lblTabelaDeClientes.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		lblTabelaDeClientes.setHorizontalAlignment(SwingConstants.CENTER);
		lblTabelaDeClientes.setBounds(59, 7, 306, 30);
		contentPane.add(lblTabelaDeClientes);

		// Excluir
		JButton btnExcluir = new JButton("Excluir");
		btnExcluir.setActionCommand("excluir");
		btnExcluir.addActionListener(gBtn);
		btnExcluir.setBounds(177, 231, 89, 23);
		contentPane.add(btnExcluir);

		// Alterar
		JButton btnAlterar = new JButton("Alterar");
		btnAlterar.setActionCommand("alterar");
		btnAlterar.addActionListener(gBtn);
		btnAlterar.setBounds(303, 231, 89, 23);
		contentPane.add(btnAlterar);

		// Voltar
		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.setActionCommand("voltar");
		btnVoltar.addActionListener(gBtn);
		btnVoltar.setBounds(42, 231, 89, 23);
		contentPane.add(btnVoltar);

		// config jframe
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
	}

	public void atualizaTabela() {
		tableModelCliente.setNumRows(0);
		for (Cliente c : ctrlCliente.getClienteDAO().getList()) {
			int codigo = c.getCodigo();
			String nome = c.getNome();
			String telefone = c.getTelefone();
			Object[] row = { codigo, nome, telefone };
			tableModelCliente.addRow(row);
		}
	}

	private class GerenciadorDeBotoes implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals("excluir")) {
				int codigo = (int) tabelaCliente.getValueAt(tabelaCliente.getSelectedRow(), 0);
				try {
					ctrlCliente.remove(codigo);
					JOptionPane.showMessageDialog(null, "Cliente excluido");
					atualizaTabela();
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
				}
			} else if (e.getActionCommand().equals("alterar")) {
				int codigo = (int) tabelaCliente.getValueAt(tabelaCliente.getSelectedRow(), 0);
				try {
					ctrlCliente.TelaAlterarProt(codigo);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
				}

			} else if (e.getActionCommand().equals("voltar")) {
				dispose();
			}
		}

	}
}
