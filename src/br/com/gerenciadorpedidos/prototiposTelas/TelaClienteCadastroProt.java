/**
 * 
 */
package br.com.gerenciadorpedidos.prototiposTelas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import br.com.gerenciadorpedidos.control.ControladorCliente;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JButton;

/**
 * @author Rodolfo Ilce Pereira Email: rodolfo.pereirailce@gmail.com 09:31:01
 */
public class TelaClienteCadastroProt extends JFrame {
	private ControladorCliente ctrlCliente;
	private GerenciadorDeBotoes gBtn;

	// componentes
	private JPanel contentPane;
	private JTextField tfNome;
	private JTextField tfTelefone;

	// /**
	// * Launch the application.
	// */
	// public static void main(String[] args) {
	// EventQueue.invokeLater(new Runnable() {
	// public void run() {
	// try {
	// TelaClienteCadastroProt frame = new TelaClienteCadastroProt();
	// frame.setVisible(true);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// });
	// }
	//
	// /**
	// * Create the frame.
	// */
	public TelaClienteCadastroProt(ControladorCliente owner) {
		super("Cliente cadastro - Gerenciador de pedidos");
		this.ctrlCliente = owner;
		this.gBtn = new GerenciadorDeBotoes();
		init();

	}

	public void init() {
		contentPane = new JPanel();
		contentPane.setBackground(new Color(139, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		// componentes
		// Nome
		JLabel lblNome = new JLabel("Nome");
		lblNome.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNome.setBounds(126, 62, 55, 35);
		contentPane.add(lblNome);

		tfNome = new JTextField();
		tfNome.setText("");
		tfNome.setBounds(191, 71, 100, 20);
		contentPane.add(tfNome);
		tfNome.setColumns(10);

		// Telefone
		JLabel lblTelefone = new JLabel("Telefone");
		lblTelefone.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblTelefone.setBounds(110, 112, 71, 35);
		contentPane.add(lblTelefone);

		tfTelefone = new JTextField();
		tfTelefone.setText("");
		tfTelefone.setBounds(191, 121, 100, 20);
		contentPane.add(tfTelefone);
		tfTelefone.setColumns(10);

		// Cancelar
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(110, 181, 100, 30);
		btnCancelar.setActionCommand("cancelar");
		btnCancelar.addActionListener(gBtn);
		contentPane.add(btnCancelar);

		// Salvar
		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.setActionCommand("salvar");
		btnSalvar.addActionListener(gBtn);
		btnSalvar.setBounds(234, 181, 100, 30);
		contentPane.add(btnSalvar);
		// config jframe
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
	}

	private void limpaCampos() {
		tfNome.setText("");
		tfTelefone.setText("");
	}

	private class GerenciadorDeBotoes implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals("salvar")) {
				try {
					ctrlCliente.recebeDados(tfNome.getText(), tfTelefone.getText());
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
				}
				JOptionPane.showMessageDialog(null, "Cliente cadastrado");
				limpaCampos();
			} else if (e.getActionCommand().equals("cancelar")) {
				limpaCampos();
				dispose();
			}
		}

	}
}
