package br.com.gerenciadorpedidos.prototiposTelas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.GridBagLayout;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Window.Type;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class TelaPedidoInicioProt extends JFrame {

	private JPanel contentPane;
	private JTable tabelaClientes;
	private JTextField tfCampoPesquisa;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaPedidoInicioProt frame = new TelaPedidoInicioProt();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaPedidoInicioProt() {
		// TEMA
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (IllegalAccessException e) {
			// TODO
		} catch (ClassNotFoundException e) {
			// TODO
		} catch (InstantiationException e) {
			// TODO
		} catch (UnsupportedLookAndFeelException e) {
			// TODO
		}
		setTitle("Pedido - Gerenciador de pedidos");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 626, 415);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{590, 0};
		gridBagLayout.rowHeights = new int[]{14, 20, 23, 253, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gridBagLayout);
		
				JLabel lbDigiteNome = new JLabel("Digite o nome do cliente");
				lbDigiteNome.setHorizontalAlignment(SwingConstants.CENTER);
				GridBagConstraints gbc_1 = new GridBagConstraints();
				gbc_1.anchor = GridBagConstraints.NORTH;
				gbc_1.insets = new Insets(0, 0, 5, 0);
				gbc_1.gridx = 0;
				gbc_1.gridy = 0;
				contentPane.add(lbDigiteNome, gbc_1);
		
				tfCampoPesquisa = new JTextField();
				GridBagConstraints gbc_2 = new GridBagConstraints();
				gbc_2.anchor = GridBagConstraints.NORTH;
				gbc_2.insets = new Insets(0, 0, 5, 0);
				gbc_2.gridx = 0;
				gbc_2.gridy = 1;
				contentPane.add(tfCampoPesquisa, gbc_2);
				tfCampoPesquisa.setColumns(10);
		
				JButton btPesquisar = new JButton("Pesquisar");
				btPesquisar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
					}
				});
				GridBagConstraints gbc_3 = new GridBagConstraints();
				gbc_3.anchor = GridBagConstraints.NORTH;
				gbc_3.insets = new Insets(0, 0, 5, 0);
				gbc_3.gridx = 0;
				gbc_3.gridy = 2;
				contentPane.add(btPesquisar, gbc_3);
		
				tabelaClientes = new JTable();
				tabelaClientes.setBackground(new Color(255, 160, 122));
				GridBagConstraints gbc = new GridBagConstraints();
				gbc.fill = GridBagConstraints.BOTH;
				gbc.gridx = 0;
				gbc.gridy = 3;
				contentPane.add(tabelaClientes, gbc);
	}
}
