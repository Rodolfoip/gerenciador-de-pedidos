/**
 * 
 */
package br.com.gerenciadorpedidos.prototiposTelas;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import br.com.gerenciadorpedidos.control.ControladorGeral;

/**
 * @author Rodolfo Ilce Pereira Email: rodolfo.pereirailce@gmail.com 16:38:43
 */
public class TelaInicioProt extends JFrame {
	private ControladorGeral ctrlGeral;
	private GerenciadorBotoes gBtn;
	//componentes
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	// public static void main(String[] args) {
	// EventQueue.invokeLater(new Runnable() {
	// public void run() {
	// try {
	// TelaInicioProt frame = new TelaInicioProt();
	// frame.setVisible(true);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// });
	// }

	/**
	 * Create the frame.
	 */
	public TelaInicioProt(ControladorGeral owner) {
		super("Tela inicio - Gerenciador de pedidos");
		this.ctrlGeral = owner;
		this.gBtn = new GerenciadorBotoes();
		init();
	}

	public void init() {
		
		contentPane = new JPanel();
		contentPane.setBackground(new Color(139, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnCliente = new JButton("Cliente");

		btnCliente.setBounds(231, 68, 150, 50);
		btnCliente.setActionCommand("cliente");
		btnCliente.addActionListener(gBtn);
		contentPane.add(btnCliente);

		JButton btnPedido = new JButton("Pedido");
		btnPedido.setBounds(231, 198, 150, 50);
		contentPane.add(btnPedido);

		JButton btnFatura = new JButton("Fatura");
		btnFatura.setBounds(231, 263, 150, 50);
		contentPane.add(btnFatura);

		JButton btnCampanha = new JButton("Campanha");

		btnCampanha.setBounds(231, 133, 150, 50);
		contentPane.add(btnCampanha);

		// Config Jframe
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 628, 411);
	}

	private class GerenciadorBotoes implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals("cliente")) {
				ctrlGeral.getCtrlCliente().telaInicioProt();
			}
		}

	}
}
