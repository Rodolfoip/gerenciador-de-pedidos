package br.com.gerenciadorpedidos.persist;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.HashMap;

import br.com.gerenciadorpedidos.model.Cliente;

public class ClienteDAO {
	private HashMap<Integer, Cliente> cacheClientes;
	private final String filename = "cliente.gpav";

	public ClienteDAO() {
		cacheClientes = new HashMap<>();
		load();
	}

	@SuppressWarnings("unchecked")
	private void load() {
		try {
			FileInputStream fin = new FileInputStream(filename);
			ObjectInputStream oit = new ObjectInputStream(fin);

			this.cacheClientes = (HashMap<Integer, Cliente>) oit.readObject();
			
			oit.close();
			fin.close();
			oit = null;
			fin = null;
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}

	public void put(Cliente cliente) throws Exception {
		if (cliente != null) {
			cacheClientes.put(cliente.getCodigo(), cliente);
			persist();
		}
	}

	public void persist() {
		try {
			FileOutputStream fout = new FileOutputStream(filename);
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(cacheClientes);
			oos.flush();
			fout.flush();
			oos.close();
			fout.close();
			oos = null;
			fout = null;
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public Cliente get(Integer codigo) {
		return cacheClientes.get(codigo);
	}

	public void remove(Integer codigo) {
		cacheClientes.remove(codigo);
		persist();
	}

	public Collection<Cliente> getList() {
		return cacheClientes.values();
	}
}
