/**
 * 
 */
package br.com.gerenciadorpedidos.persist;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.HashMap;

import br.com.gerenciadorpedidos.model.Campanha;

/**
 * @author Rodolfo Ilce Pereira Email: rodolfo.pereirailce@gmail.com 14:48:14
 */
public class CampanhaDAO {
	private HashMap<Integer, Campanha> cacheCampanhas;
	private final String filename = "campanha.gpav";

	public CampanhaDAO() {
		cacheCampanhas = new HashMap<>();
		load();
	}

	@SuppressWarnings("unchecked")
	private void load() {
		try {
			FileInputStream fin = new FileInputStream(filename);
			ObjectInputStream oit = new ObjectInputStream(fin);
			this.cacheCampanhas = (HashMap<Integer, Campanha>) oit.readObject();
			oit.close();
			fin.close();
			oit = null;
			fin = null;
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void put(Campanha campanha) {
		if (campanha != null) {
			cacheCampanhas.put(campanha.getNumero(), campanha);
			persist();
		}
	}

	public void persist() {
		try {
			FileOutputStream fout = new FileOutputStream(filename);
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(cacheCampanhas);
			oos.flush();
			fout.flush();
			oos.close();
			fout.close();
			oos = null;
			fout = null;
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public Campanha get(Integer numero) {
		return cacheCampanhas.get(numero);
	}

	public void remove(Integer numero) {
		cacheCampanhas.remove(numero);
		persist();
	}

	public Collection<Campanha> getList() {
		return cacheCampanhas.values();
	}
}
