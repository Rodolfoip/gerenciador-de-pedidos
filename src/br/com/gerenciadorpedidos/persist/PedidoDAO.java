package br.com.gerenciadorpedidos.persist;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.HashMap;

import br.com.gerenciadorpedidos.model.Pedido;

public class PedidoDAO {
	private HashMap<Integer, Pedido> cachePedidos;
	private final String filename = "pedido.gpav";

	public PedidoDAO() {
		cachePedidos = new HashMap<>();
		load();
	}

	@SuppressWarnings("unchecked")
	private void load() {
		try {
			FileInputStream fin = new FileInputStream(filename);
			ObjectInputStream oit = new ObjectInputStream(fin);
			this.cachePedidos = (HashMap<Integer, Pedido>) oit.readObject();
			oit.close();
			fin.close();
			oit = null;
			fin = null;
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void put(Pedido pedido) throws Exception {
		if (pedido != null) {
			cachePedidos.put(pedido.getCodigo(), pedido);
			persist();
		}

	}

	public void persist() {
		try {
			FileOutputStream fout = new FileOutputStream(filename);
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(cachePedidos);
			oos.flush();
			fout.flush();
			oos.close();
			fout.close();
			oos = null;
			fout = null;
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public Pedido get(Integer codigo) {
		return cachePedidos.get(codigo);
	}

	public void remove(Integer codigo) {
		cachePedidos.remove(codigo);
		persist();
	}

	public Collection<Pedido> getList() {
		return cachePedidos.values();
	}
}
