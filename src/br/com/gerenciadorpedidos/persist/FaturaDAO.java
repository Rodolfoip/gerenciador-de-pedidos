package br.com.gerenciadorpedidos.persist;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.HashMap;

import br.com.gerenciadorpedidos.model.Fatura;

public class FaturaDAO {
	private HashMap<Integer, Fatura> cacheFaturas;
	private final String filename = "fatura.gpav";

	public FaturaDAO() {
		cacheFaturas = new HashMap<>();
		load();
	}

	@SuppressWarnings("unchecked")
	private void load() {
		try {
			FileInputStream fin = new FileInputStream(filename);
			ObjectInputStream oit = new ObjectInputStream(fin);
			this.cacheFaturas = (HashMap<Integer, Fatura>) oit.readObject();
			oit.close();
			fin.close();
			oit = null;
			fin = null;
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void put(Fatura fatura) throws Exception {
		if (fatura != null) {
			cacheFaturas.put(fatura.getCodigo(), fatura);
			persist();
		}
	}

	public void persist() {
		try {
			FileOutputStream fout = new FileOutputStream(filename);
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(cacheFaturas);
			oos.flush();
			fout.flush();
			oos.close();
			fout.close();
			oos = null;
			fout = null;
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public Fatura get(Integer codigo) {
		return cacheFaturas.get(codigo);
	}

	public void remove(Integer codigo) {
		cacheFaturas.remove(codigo);
		persist();
	}

	public Collection<Fatura> getList() {
		return cacheFaturas.values();
	}
}
