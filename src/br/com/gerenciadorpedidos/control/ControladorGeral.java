package br.com.gerenciadorpedidos.control;

import br.com.gerenciadorpedidos.prototiposTelas.TelaInicioProt;
import br.com.gerenciadorpedidos.view.TelaInicio;

public class ControladorGeral {
    private static ControladorGeral instance;
    private ControladorCliente ctrlCliente = ControladorCliente.getInstance();
    private ControladorCampanha ctrlCampanha = ControladorCampanha.getInstance();
    private ControladorFatura ctrlFatura = ControladorFatura.getInstance();
    private ControladorPedido ctrlPedido = ControladorPedido.getInstance();

    //Tela
    private TelaInicio telaInicio;
    //Tela prototipo
    private TelaInicioProt telaInicioProt;

    public ControladorGeral(){
        this.telaInicio = new TelaInicio(this);
        this.telaInicioProt = new TelaInicioProt(this);
    }
    public static ControladorGeral getInstance(){
        if(instance == null){
            instance = new ControladorGeral();
        }
        return instance;
    }

    public ControladorCliente getCtrlCliente() {
        return ctrlCliente;
    }

    public ControladorCampanha getCtrlCampanha() {
        return ctrlCampanha;
    }

    public ControladorPedido getCtrlPedido() {
        return ctrlPedido;
    }

    public ControladorFatura getCtrlFatura() {
        return ctrlFatura;
    }
    
    //tela
    public void inicia(){
        telaInicio.init();
        telaInicio.setVisible(true);
    }
    public void iniciaProt() {
    	telaInicioProt.init();
    	telaInicioProt.setVisible(true);
    }
}
