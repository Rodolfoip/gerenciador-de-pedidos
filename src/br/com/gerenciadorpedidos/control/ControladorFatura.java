/**
 * 
 */
package br.com.gerenciadorpedidos.control;

/**
 * @author Rodolfo Ilce Pereira Email: rodolfo.pereirailce@gmail.com 15:02:53
 */
public class ControladorFatura {
    private static ControladorFatura instance;

    public static ControladorFatura getInstance(){
        if(instance == null){
            instance = new ControladorFatura();
        }
        return instance;
    }
}
