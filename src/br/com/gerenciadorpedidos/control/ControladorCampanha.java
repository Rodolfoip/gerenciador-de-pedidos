/**
 * 
 */
package br.com.gerenciadorpedidos.control;

/**
 * @author Rodolfo Ilce Pereira
 * Email: rodolfo.pereirailce@gmail.com
 * 15:02:41
 */
public class ControladorCampanha {
    private static ControladorCampanha instance;

    public static ControladorCampanha getInstance(){
        if(instance == null){
            instance = new ControladorCampanha();
        }
        return instance;
    }
}
