/**
 * 
 */
package br.com.gerenciadorpedidos.control;

import br.com.gerenciadorpedidos.model.Cliente;
import br.com.gerenciadorpedidos.persist.ClienteDAO;
import br.com.gerenciadorpedidos.prototiposTelas.TelaClienteAlteraProt;
import br.com.gerenciadorpedidos.prototiposTelas.TelaClienteCadastroProt;
import br.com.gerenciadorpedidos.prototiposTelas.TelaClienteInicioProt;
import br.com.gerenciadorpedidos.prototiposTelas.TelaClienteTabelaProt;
import br.com.gerenciadorpedidos.view.cliente.TelaClienteCadastro;
import br.com.gerenciadorpedidos.view.cliente.TelaClienteInicio;
import br.com.gerenciadorpedidos.view.cliente.TelaClienteLista;
import sun.print.PeekGraphics;

/**
 * @author Rodolfo Ilce Pereira Email: rodolfo.pereirailce@gmail.com 15:02:12
 */
public class ControladorCliente {
	private ClienteDAO clienteDAO;
	private static ControladorCliente instance;
	// telas
	private TelaClienteInicio telaInicio;
	private TelaClienteCadastro telaCadastro;
	private TelaClienteLista telaTabelaCliente;
	// telasPrototipo
	private TelaClienteInicioProt telaInicioProt;
	private TelaClienteCadastroProt telaCadastroProt;
	private TelaClienteTabelaProt telaTabelaProt;
	private TelaClienteAlteraProt telaAlterarProt;

	public ClienteDAO getClienteDAO() {
		if (clienteDAO == null) {
			return new ClienteDAO();
		}
		return clienteDAO;
	}

	// singleton
	private ControladorCliente() {
		this.clienteDAO = new ClienteDAO();
		this.telaCadastro = new TelaClienteCadastro(this);
		this.telaInicio = new TelaClienteInicio(this);
		this.telaTabelaCliente = new TelaClienteLista(this);
		// tela prototipo
		this.telaInicioProt = new TelaClienteInicioProt(this);
		this.telaCadastroProt = new TelaClienteCadastroProt(this);
		this.telaTabelaProt = new TelaClienteTabelaProt(this);
		this.telaAlterarProt = new TelaClienteAlteraProt(this);
	}

	public static synchronized ControladorCliente getInstance() {
		if (instance == null) {
			instance = new ControladorCliente();
		}
		return instance;
	}

	// fim singleton
	public void recebeDados(String nome, String telefone) throws Exception {
		int codigo = geraCodigoCliente();
		Cliente cliente = new Cliente(codigo, nome, telefone);
		if (cliente != null) {
			cadastraCliente(cliente);
		}

	}

	public void cadastraCliente(Cliente cliente) throws Exception {
		if (cliente != null) {
			this.clienteDAO.put(cliente);
		}
	}

	private int geraCodigoCliente() throws Exception {
		int codigo = 0;
		do {
			codigo++;
		} while (existeCliente(codigo));
		return codigo;
	}

	public void altera(int codigo, String nome, String telefone) throws Exception {
		Cliente cliente = encontraClientePorCodigo(codigo);
		if (cliente != null) {
			cliente.setNome(nome);
			cliente.setTelefone(telefone);
			this.clienteDAO.put(cliente);
		}
	}

	public void remove(int codigo) {
		if (existeCliente(codigo)) {
			clienteDAO.remove(codigo);
		}
	}

	private boolean existeCliente(int codigo) {
		Cliente cliente = clienteDAO.get(new Integer(codigo));
		if (cliente != null) {
			return true;
		}
		return false;
	}

	public Cliente encontraClientePorCodigo(int codigo) throws Exception {
		if (existeCliente(codigo)) {
			Cliente cliente = clienteDAO.get(new Integer(codigo));
			return cliente;
		}
		throw new Exception("Cliente n�o existe");
	}

	public String exibeClientes() {
		String lista = "";
		if (!this.clienteDAO.getList().isEmpty()) {
			for (Cliente c : this.clienteDAO.getList()) {
				lista += "======================================\n";
				lista += "Codigo: " + c.getCodigo() + "\n";
				lista += "Nome: " + c.getNome() + "\n";
				lista += "Telefone: " + c.getTelefone() + "\n";
				lista += "======================================\n";
			}
		}
		return lista;
	}

	// Metodos de tela
	public void telaInicio() {
		telaInicio.init();
		telaInicio.setVisible(true);
	}

	public void telaCadastro() {
		telaCadastro.init();
		telaCadastro.setVisible(true);
	}

	public void telaTabela() {

	}

	// metodos tela prototipo
	public void telaInicioProt() {
		telaInicioProt.init();
		telaInicioProt.setVisible(true);
	}

	public void telaCadastroProt() {
		telaCadastroProt.init();
		telaCadastroProt.setVisible(true);
	}

	public void telaTabelaProt() {
		telaTabelaProt.init();
		telaTabelaProt.setVisible(true);
	}

	public void TelaAlterarProt(int codigo) throws Exception {
		telaAlterarProt.init();
		telaAlterarProt.pegaDados(codigo);
		telaAlterarProt.setVisible(true);
	}
}
