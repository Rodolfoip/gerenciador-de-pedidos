package br.com.gerenciadorpedidos.control;

import br.com.gerenciadorpedidos.view.pedido.TelaPedidoInicio;

/**
 * @author Rodolfo Ilce Pereira
 * Email: rodolfo.pereirailce@gmail.com
 * 15:02:27
 */
public class ControladorPedido {
    private static ControladorPedido instance;

    //Tela
    private TelaPedidoInicio telaInicio;

    private ControladorPedido(){
        this.telaInicio = new TelaPedidoInicio(this);
    }
    public static ControladorPedido getInstance(){
        if(instance == null){
            instance = new ControladorPedido();
        }
        return instance;
    }
    //metodos tela
    public void telaInicio(){
        telaInicio.init();
        telaInicio.setVisible(true);
    }

}
